# ci-cd-demo-app
## Project was made to support those articles:
- [Create CI/CD pipeline in GitLab with AWS CDK, Docker, Spring Boot and Gradle](https://brightinventions.pl/blog/create-ci-cd-pipeline-in-gitlab-with-aws-cdk-docker-spring-boot-and%C2%A0gradle)
- [Add the RDS database to a Spring Boot app with AWS CDK](https://brightinventions.pl/blog/add-the-rds-database-to-a-spring-boot-app-with-aws-cdk)

---

This project demonstrates the use of a CI/CD pipeline with GitLab to deploy an RDS database and a Fargate service with a Spring Boot application on AWS. The deployment is accomplished using the AWS Cloud Development Kit (CDK).

Here's an overview of the components and features of this project:

CI/CD pipeline: The pipeline is configured in GitLab and consists of multiple stages, including building, testing, and deploying the application. The pipeline is triggered by code changes and for master branch automatically deploys the application to AWS on stage environment. Deploy on prod can be trigerred manually.

RDS database: The RDS database is deployed using the CDK and is configured to store data for the Spring Boot application.

Fargate service: The Fargate service is also deployed using the CDK and is responsible for running and scaling the Spring Boot application.

Spring Boot application: The application is a simple Spring Boot app that uses the RDS database to store and retrieve data.
