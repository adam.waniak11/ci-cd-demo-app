import {Duration, RemovalPolicy, Stack} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {InstanceClass, InstanceSize, InstanceType, IVpc} from "aws-cdk-lib/aws-ec2";
import {Credentials, DatabaseInstance, DatabaseInstanceEngine, PostgresEngineVersion} from "aws-cdk-lib/aws-rds";
import {deployEnv, isProductionDeployEnv, KnownDeployEnv, projectEnvSpecificName} from "./env-utils";

export class DatabaseStack extends Stack {
    static readonly databasePort = 5432;
    static readonly databaseName = `backend_postgres_db`;

    readonly databaseInstance: DatabaseInstance;

    constructor(scope: Construct, id: string, vpc: IVpc) {
        super(scope, id);

        const databaseUsername = 'backend_postgres_db_user';
        const dbCredentialsSecretName = `backend_db_postgres_credentials_${deployEnv()}`;

        const databaseCredentials = Credentials.fromGeneratedSecret(databaseUsername, {
            secretName: dbCredentialsSecretName
        });

        this.databaseInstance = new DatabaseInstance(this, projectEnvSpecificName('postgres-db'), {
            databaseName: DatabaseStack.databaseName,
            engine: DatabaseInstanceEngine.postgres({version: PostgresEngineVersion.VER_14_2}),
            instanceType: InstanceType.of(InstanceClass.T3, InstanceSize.MICRO),
            instanceIdentifier: projectEnvSpecificName('postgres-db'),
            credentials: databaseCredentials,
            port: DatabaseStack.databasePort,
            maxAllocatedStorage: 200,
            vpc,
            deletionProtection: deployEnv() === KnownDeployEnv.prod,
            removalPolicy: removalPolicyAppropriateForEnv(),
            backupRetention: databaseBackupRetentionDaysForEnv(),
            copyTagsToSnapshot: true,
            iamAuthentication: true
        });
    }
}

export function removalPolicyAppropriateForEnv() {
  return isProductionDeployEnv() ? RemovalPolicy.RETAIN : RemovalPolicy.DESTROY;
}

export function databaseBackupRetentionDaysForEnv() {
  return isProductionDeployEnv() ? Duration.days(14) : Duration.days(1)
}

