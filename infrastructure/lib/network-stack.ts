import {Stack, StackProps} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {IVpc, Vpc} from "aws-cdk-lib/aws-ec2";
import {projectEnvSpecificName} from "./env-utils";

export class NetworkStack extends Stack {
    readonly vpc: IVpc;

    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        this.vpc = new Vpc(this, projectEnvSpecificName("VPC"), {
            natGateways: 1
        })
    }
}
