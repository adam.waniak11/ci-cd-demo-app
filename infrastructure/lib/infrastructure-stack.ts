import {Duration, RemovalPolicy, Stack, StackProps} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {AwsLogDriver, Cluster, ContainerImage, Secret} from "aws-cdk-lib/aws-ecs";
import {ApplicationLoadBalancedFargateService} from "aws-cdk-lib/aws-ecs-patterns";
import {LogGroup} from "aws-cdk-lib/aws-logs";
import {IVpc, Port} from "aws-cdk-lib/aws-ec2";
import * as ecr from "aws-cdk-lib/aws-ecr";
import {projectEnvSpecificName} from "./env-utils";
import {DatabaseInstance} from "aws-cdk-lib/aws-rds";
import {DatabaseStack} from "./database-stack";

export class InfrastructureStack extends Stack {

    private readonly TAG_COMMIT: string = process.env.TAG_COMMIT || 'latest'
    private readonly ECR_REPOSITORY_NAME: string = "ci-cd-demo-app"

    constructor(scope: Construct, id: string, vpc: IVpc, dbInstance: DatabaseInstance, props?: StackProps) {
        super(scope, id, props);

        const cluster = new Cluster(this, projectEnvSpecificName('Cluster'), {
            vpc: vpc
        });

        const service: ApplicationLoadBalancedFargateService = new ApplicationLoadBalancedFargateService(this, projectEnvSpecificName("application-lb-fargate-service"), {
            serviceName: projectEnvSpecificName("fargate-service"),
            cluster: cluster,
            cpu: 512,
            desiredCount: 2,
            listenerPort: 8080,
            memoryLimitMiB: 1024,
            publicLoadBalancer: true,
            taskImageOptions: {
                containerName: projectEnvSpecificName("ecs-container"),
                image: ContainerImage.fromEcrRepository(ecrRepositoryForService(this, this.ECR_REPOSITORY_NAME), this.TAG_COMMIT),
                containerPort: 8080,
                secrets: {
                    DB_POSTGRES_PASSWORD: Secret.fromSecretsManager(dbInstance.secret!, "password"),
                    DB_POSTGRES_USERNAME: Secret.fromSecretsManager(dbInstance.secret!, "username"),
                },
                environment: {
                    DB_POSTGRES_HOST: dbInstance.dbInstanceEndpointAddress,
                    DB_POSTGRES_PORT: dbInstance.dbInstanceEndpointPort,
                    DB_POSTGRES_NAME: DatabaseStack.databaseName,
                },
                logDriver: new AwsLogDriver({
                    logGroup: new LogGroup(this, projectEnvSpecificName("log-group"), {
                        logGroupName: projectEnvSpecificName("app-service"),
                        removalPolicy: RemovalPolicy.DESTROY
                    }),
                    streamPrefix: projectEnvSpecificName(),
                })
            }
        })

        service.service.connections.allowTo(dbInstance.connections, Port.tcp(DatabaseStack.databasePort), "Postgres db connection")

        service.targetGroup.configureHealthCheck({
            path: "/actuator/health",
            port: "8080",
            healthyHttpCodes: "200"
        })

        const scalableTaskCount = service.service.autoScaleTaskCount({
            minCapacity: 2,
            maxCapacity: 4
        });

        scalableTaskCount.scaleOnCpuUtilization(projectEnvSpecificName("service-auto-scaling"), {
            targetUtilizationPercent: 50,
            scaleInCooldown: Duration.seconds(60),
            scaleOutCooldown: Duration.seconds(60),
        })
    }
}

export function ecrRepositoryForService(scope: Construct, serviceName: string) {
    return ecr.Repository.fromRepositoryName(scope, `${serviceName} repository`, serviceName)
}
